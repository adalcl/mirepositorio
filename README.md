
![Bitbucket](https://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Atlassian_Bitbucket_Logo.png/1600px-Atlassian_Bitbucket_Logo.png) 

# Creación y subida de repositorio en [Bitbucket](https://bitbucket.org/)

## Creación de archivos

  El primer paso es crear la carpeta "repositorio", crear nuestro archivo index.html y nuestro archivo index.js para su posterior subida. 

  1. Elegir nuestra unidad o donde querramos guardar nuestros archivos con  `$ cd miUbicacion`y una vez dentro, con la consola `$ mkdir "nombreRepositorio` para crear nuestra carpeta que contendrá los archivos

  2. Introcudir el comando `$ ls` y comprobar que se ha creado nuestra carpeta e ingresar a la misma

  3. Entramos con  `$ cd nombreCarpeta`

  ## Añadir el proyecto al control de versiones de git una vez dentro del directorio

  + `$ git init`

  ## Establecer nuestro usuario y direccion de correo

  +  `$ git config --global user.email "mi_correo@mail.com"` 
    
  + `$ git config --global user.name "nombre_usuario"`

  ## Crear nuestros archivos y comprobar que se han creado y comprbar el estado

  + `$ touch index.html`
  + `$ touch index.js`
  + `$ls` 
  + `$ git status` 
  
  Debe aparecer un mensaje: No commits yetUntracked files:
  (use "git add <file>..." to include in what will be committed)
        index.html
        index.js


  ## El seguiente paso será añadir un archivo al área de ensayos para ver la diferencia

  + `git add index.html`
  +  `$ git status`

  ## A continuación nos aparecera
 
 
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   index.html

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        index.js

## Añadimos el index.js y comprobamos que los los estén listos para el commit con un git status

On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   index.html
        new file:   index.js
## Modificamos el archivo index.html con VIM y después de guardar los cambios vemos el estado y cómo le ha afectado a nuestro archivo

+ `$ vim index.html`

+ `$ git status`

On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   index.html
        new file:   index.js

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   index.html

## Descartar o guerdar cambios
        
 Tal y como nos lo dice el mensaje podemos descartar esos cambios o bien hacer un git add para actualizar nuestro archivo y que esté listo para el commit y repetimos los pasos con el archivo js, una vez listos registramos los cambios en el historial

+ `git commit -m "Comentario del commit"`

## Comprobamos que todo esté bien

+ `$ git status`

## Para confirmar los cambios y subir nuestros archivos, en la misma web de Bitbucket nos dan los pasos, desde la HTTPS con dos pasos más 

+ `$ git remote add origin https:......`

+ `$ git push -u origin master`

## Último paso 

Nos pedirá que nos autentiquemos con nuestro usuario y contraseña que previamente hemos creado o modificado en nuestra cuenta de [Bitbucket](https://bitbucket.org/), si la autenticación a sido correcta, nos habrá subido nuestro repositorio y nos debe aparecer por consola que todo ha ido bien, con una mensaje como el que se muestra a continuación:

Enumerating objects: 4, done.
Counting objects: 100% (4/4), done
Delta compression using up to 4 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 10.01 KiB | 5.00 MiB/s, done.
Total 4 (delta 1), reused 0 (delta 0), pack-reused 0
...




![Bitbucket](https://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Atlassian_Bitbucket_Logo.png/1600px-Atlassian_Bitbucket_Logo.png) 






